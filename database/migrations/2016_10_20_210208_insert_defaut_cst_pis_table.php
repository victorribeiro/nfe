<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDefautCstPisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("INSERT INTO cst_pis (`code`, `name`, `created_at`, `updated_at`) VALUES
            ('01', 'Operação Tributável com Alíquota Básica', NOW(), NOW()),
            ('02', 'Operação Tributável com Alíquota Diferenciada', NOW(), NOW()),
            ('03', 'Operação Tributável com Alíquota por Unidade de Medida de Produto', NOW(), NOW()),
            ('04', 'Operação Tributável Monofásica - Revenda a Alíquota Zero', NOW(), NOW()),
            ('05', 'Operação Tributável por Substituição Tributária', NOW(), NOW()),
            ('06', 'Operação Tributável a Alíquota Zero', NOW(), NOW()),
            ('07', 'Operação Isenta da Contribuição', NOW(), NOW()),
            ('08', 'Operação sem Incidência da Contribuição', NOW(), NOW()),
            ('09', 'Operação com Suspensão da Contribuição', NOW(), NOW()),
            ('49', 'Outras Operações de Saída', NOW(), NOW()),
            ('50', 'Operação com Direito a Crédito - Vinculada Exclusivamente a Receita Tributada no Mercado Interno', NOW(), NOW()),
            ('51', 'Operação com Direito a Crédito - Vinculada Exclusivamente a Receita Não-Tributada no Mercado Interno', NOW(), NOW()),
            ('52', 'Operação com Direito a Crédito - Vinculada Exclusivamente a Receita de Exportação', NOW(), NOW()),
            ('53', 'Operação com Direito a Crédito - Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno', NOW(), NOW()),
            ('54', 'Operação com Direito a Crédito - Vinculada a Receitas Tributadas no Mercado Interno e de Exportação', NOW(), NOW()),
            ('55', 'Operação com Direito a Crédito - Vinculada a Receitas Não Tributadas no Mercado Interno e de Exportação', NOW(), NOW()),
            ('56', 'Operação com Direito a Crédito - Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno e de Exportação', NOW(), NOW()),
            ('60', 'Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita Tributada no Mercado Interno', NOW(), NOW()),
            ('61', 'Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita Não-Tributada no Mercado Interno', NOW(), NOW()),
            ('62', 'Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita de Exportação', NOW(), NOW()),
            ('63', 'Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno', NOW(), NOW()),
            ('64', 'Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas no Mercado Interno e de Exportação', NOW(), NOW()),
            ('65', 'Crédito Presumido - Operação de Aquisição Vinculada a Receitas Não-Tributadas no Mercado Interno e de Exportação', NOW(), NOW()),
            ('66', 'Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno e de Exportação', NOW(), NOW()),
            ('67', 'Crédito Presumido - Outras Operações', NOW(), NOW()),
            ('70', 'Operação de Aquisição sem Direito a Crédito', NOW(), NOW()),
            ('71', 'Operação de Aquisição com Isenção', NOW(), NOW()),
            ('72', 'Operação de Aquisição com Suspensão', NOW(), NOW()),
            ('73', 'Operação de Aquisição a Alíquota Zero', NOW(), NOW()),
            ('74', 'Operação de Aquisição sem Incidência da Contribuição', NOW(), NOW()),
            ('75', 'Operação de Aquisição por Substituição Tributária', NOW(), NOW()),
            ('98', 'Outras Operações de Entrada', NOW(), NOW()),
            ('99', 'Outras Operações', NOW(), NOW());"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('cst_pis')->truncate();
    }
}
