<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('kind_person', [ 'pf', 'pj' ])->nullable();
            $table->string('name');
            $table->string('document')->nullable();
            $table->date('municipal_registration')->nullable();
            $table->date('state_registration')->nullable();
            $table->string('cep', 8)->nullable();
            $table->text('street')->nullable();
            $table->string('number', 10)->nullable();
            $table->string('neighborhood')->nullable();
            $table->string('complement')->nullable();
            $table->string('city', 80)->nullable();
            $table->string('state', 2)->nullable();
            $table->string('contact_name');
            $table->string('contact_email');
            $table->string('contact_phone')->nullable();
            $table->string('counter_name')->nullable();
            $table->string('counter_email')->nullable();
            $table->string('counter_phone')->nullable();
            $table->enum('environment', [ 'homologação', 'produção' ])->nullable();
            $table->string('tax_regime')->nullable(); //Regime tributário
            $table->string('nfce_token_csc')->nullable();
            $table->string('nfce_csc')->nullable();
            $table->string('nfce_serial_printer')->nullable();
            $table->integer('nfce_cfop_id')->unsigned()->nullable();
            $table->foreign('nfce_cfop_id')->references('id')->on('cfops');
            $table->string('certificate_a1')->nullable();
            $table->string('certificate_a1_password')->nullable();
            $table->string('logo')->nullable();
            $table->string('integration_key')->nullable();
            $table->integer('plan_id')->unsigned()->nullable();
            $table->foreign('plan_id')->references('id')->on('plans');
            $table->string('card_number')->nullable();
            $table->string('expiration_date')->nullable();
            $table->string('cv')->nullable();
            $table->string('card_name')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
