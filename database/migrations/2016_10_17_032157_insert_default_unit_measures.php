<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDefaultUnitMeasures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("INSERT INTO unit_measures (`name`, `created_at`, `updated_at`) VALUES
            ('Balde', NOW(), NOW()),
            ('Barras', NOW(), NOW()),
            ('Barrica', NOW(), NOW()),
            ('Big Bag', NOW(), NOW()),
            ('Bobina', NOW(), NOW()),
            ('Bombonas', NOW(), NOW()),
            ('Caixa', NOW(), NOW()),
            ('Centimetros', NOW(), NOW()),
            ('Cento', NOW(), NOW()),
            ('Chapa', NOW(), NOW()),
            ('Conjunto', NOW(), NOW()),
            ('Display', NOW(), NOW()),
            ('Fardo', NOW(), NOW()),
            ('Frasco', NOW(), NOW()),
            ('Galão', NOW(), NOW()),
            ('Grama', NOW(), NOW()),
            ('Hectares', NOW(), NOW()),
            ('Hora', NOW(), NOW()),
            ('Kilograma', NOW(), NOW()),
            ('Kilometros', NOW(), NOW()),
            ('Litro', NOW(), NOW()),
            ('Livre', NOW(), NOW()),
            ('Metros', NOW(), NOW()),
            ('Metros Cúbicos', NOW(), NOW()),
            ('Metros Lineares', NOW(), NOW()),
            ('Metros Quadrados', NOW(), NOW()),
            ('Milheiro', NOW(), NOW()),
            ('Mililitro', NOW(), NOW()),
            ('Pack', NOW(), NOW()),
            ('Pacote', NOW(), NOW()),
            ('Par', NOW(), NOW()),
            ('Peça', NOW(), NOW()),
            ('Polegadas', NOW(), NOW()),
            ('Rolo', NOW(), NOW()),
            ('Saco', NOW(), NOW()),
            ('Segundo', NOW(), NOW()),
            ('Tonelada', NOW(), NOW()),
            ('Unidade', NOW(), NOW());"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('unit_measures');
    }
}
