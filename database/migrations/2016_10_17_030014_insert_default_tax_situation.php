<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDefaultTaxSituation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("INSERT INTO tax_situations (`code`, `name`, `tax_regime`, `created_at`, `updated_at`) VALUES
            ('00', 'Tributada integralmente', 'normal', NOW(), NOW()),
            ('10', 'Tributada e com cobrança do ICMS por substituição tributária', 'normal', NOW(), NOW()),
            ('20', 'Com redução de base de cálculo', 'normal', NOW(), NOW()),
            ('30', 'Isenta ou não tributada e com cobrança do ICMS por substituição tributária', 'normal', NOW(), NOW()),
            ('40', 'Isenta', 'normal', NOW(), NOW()),
            ('41', 'Não tributada', 'normal', NOW(), NOW()),
            ('50', 'Suspensão', 'normal', NOW(), NOW()),
            ('51', 'Diferimento', 'normal', NOW(), NOW()),
            ('60', 'ICMS cobrado anteriormente por substituição tributária', 'normal', NOW(), NOW()),
            ('70', 'Com redução de base de cálculo e cobrança do ICMS por substituição tributária', 'normal', NOW(), NOW()),
            ('90', 'Outras', 'normal', NOW(), NOW()),
            ('102', 'Tributada pelo Simples Nacional sem permissão de crédito', 'simples', NOW(), NOW()),
            ('103', 'Isenção do ICMS no Simples Nacional para faixa de receita bruta', 'simples',  NOW(), NOW()),
            ('201', 'Tributada pelo Simples Nacional com permissão de crédito e com cobrança do ICMS por substituição tributária', 'simples',  NOW(), NOW()),
            ('202', 'Tributada pelo Simples Nacional sem permissão de crédito e com cobrança do ICMS por substituição tributária', 'simples',  NOW(), NOW()),
            ('203', 'Isenção do ICMS no Simples Nacional para faixa de receita bruta e com cobrança do ICMS por substituição tributária', 'simples',  NOW(), NOW()),
            ('300', 'Imune', 'simples',  NOW(), NOW()),
            ('400', 'Não tributada pelo Simples Nacional', 'simples',  NOW(), NOW()),
            ('500', 'ICMS cobrado anteriormente por substituição tributária (substituído) ou por antecipação', 'simples',  NOW(), NOW()),
            ('900', 'Outros', 'simples',  NOW(), NOW());"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('tax_situations')->truncate();
    }
}
