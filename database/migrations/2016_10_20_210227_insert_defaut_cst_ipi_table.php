<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDefautCstIpiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("INSERT INTO cst_ipis (`code`, `name`, `created_at`, `updated_at`) VALUES
            ('00', 'Entrada com Recuperação de Crédito', NOW(), NOW()),
            ('01', 'Entrada Tributável com Alíquota Zero', NOW(), NOW()),
            ('02', 'Entrada Isenta', NOW(), NOW()),
            ('03', 'Entrada Não-Tributada', NOW(), NOW()),
            ('04', 'Entrada Imune', NOW(), NOW()),
            ('05', 'Entrada com Suspensão', NOW(), NOW()),
            ('49', 'Outras Entradas', NOW(), NOW()),
            ('50', 'Saída Tributada', NOW(), NOW()),
            ('51', 'Saída Tributável com Alíquota Zero', NOW(), NOW()),
            ('52', 'Saída Isenta', NOW(), NOW()),
            ('53', 'Saída Não-Tributada', NOW(), NOW()),
            ('54', 'Saída Imune', NOW(), NOW()),
            ('55', 'Saída com Suspensão', NOW(), NOW()),
            ('99', 'Outras Saídas', NOW(), NOW());"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('cst_ipis')->truncate();
    }
}
