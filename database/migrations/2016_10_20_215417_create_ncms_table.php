<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNcmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ncms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->string('ncm');
            $table->integer('cfop_id')->unsigned()->index()->nullable();
            $table->foreign('cfop_id')->references('id')->on('cfops');
            $table->integer('tax_situation_id')->unsigned()->index()->nullable();
            $table->foreign('tax_situation_id')->references('id')->on('tax_situations');
            $table->integer('state_id')->unsigned()->nullable();
            $table->foreign('state_id')->references('id')->on('states');
            $table->float('aliquot_icms')->nullable();
            $table->float('aliquot_icms_st')->nullable();
            $table->float('aliquot_mva')->nullable();
            $table->float('reduction_percentage_bc')->nullable();
            $table->float('internal_aliquot_target')->nullable();
            $table->float('reduction_percentage_bc_st')->nullable();
            $table->float('percentage_acceptance')->nullable();
            $table->float('credit_percentage_icms')->nullable();
            $table->float('aliquot_pis')->nullable();
            $table->integer('cst_pis_id')->unsigned()->index()->nullable();
            $table->foreign('cst_pis_id')->references('id')->on('cst_pis');
            $table->float('aliquot_cofins')->nullable();
            $table->integer('cst_cofins_id')->unsigned()->index()->nullable();
            $table->foreign('cst_cofins_id')->references('id')->on('cst_pis');
            $table->float('aliquot_ipi')->nullable();
            $table->integer('cst_ipi_id')->unsigned()->index()->nullable();
            $table->foreign('cst_ipi_id')->references('id')->on('cst_ipis');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ncms');
    }
}
