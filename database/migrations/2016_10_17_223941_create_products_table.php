<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->string('name');
            $table->string('code')->nullable();
            $table->float('value');
            $table->string('ncm')->nullable();
            $table->integer('category_id')->unsigned()->index();
            $table->foreign('category_id')->references('id')->on('categories');
            $table->integer('unit_measure_id')->unsigned()->index();
            $table->foreign('unit_measure_id')->references('id')->on('unit_measures');
            $table->integer('cfop_id')->unsigned()->index()->nullable();
            $table->foreign('cfop_id')->references('id')->on('cfops');
            $table->integer('tax_situation_id')->unsigned()->index()->nullable();
            $table->foreign('tax_situation_id')->references('id')->on('tax_situations');
            $table->string('code_anp')->nullable();
            $table->string('image')->nullable();
            $table->integer('product_origin_id')->unsigned()->index()->nullable();
            $table->foreign('product_origin_id')->references('id')->on('product_origins');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
