<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDefaultProductsOrigins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("INSERT INTO product_origins (`code`, `name`, `created_at`, `updated_at`) VALUES
            ('0', 'Nacional, exceto as indicadas nos códigos 3 a 5', NOW(), NOW()),
            ('1', 'Estrangeira - Importação direta, exceto a indicada no código 6', NOW(), NOW()),
            ('2', 'Estrangeira - Adquirida no mercado interno, exceto a indicada no código 7', NOW(), NOW()),
            ('3', 'Nacional, mercadoria ou bem com Conteúdo de Importação superior a 40% (quarenta por cento)', NOW(), NOW()),
            ('4', 'Nacional, cuja produção tenha sido feita em conformidade com os processos produtivos básicos de que tratam o Decreto-Lei nº 288/67, e as Leis nºs 8.248/91, 8.387/91, 10.176/01 e 11.484/07', NOW(), NOW()),
            ('5', 'Nacional, mercadoria ou bem com Conteúdo de Importação inferior ou igual a 40% (quarenta por cento)', NOW(), NOW()),
            ('6', 'Estrangeira - Importação direta, sem similar nacional, constante em lista de Resolução CAMEX', NOW(), NOW()),
            ('7', 'Estrangeira - Adquirida no mercado interno, sem similar nacional, constante em lista de Resolução CAMEX', NOW(), NOW());"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('product_origins')->truncate();
    }
}
