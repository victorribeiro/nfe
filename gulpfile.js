var elixir = require('laravel-elixir');

var assets_path = 'resources/assets/';
var bower_path = 'vendor/bower_components/';

elixir(function(mix) {
  mix
  
  // Font Awesome
    .copy(bower_path + '/font-awesome/fonts', 'public/fonts/font-awesome')
    .copy(bower_path + 'bootstrap-fileinput/img', 'public/img')
    .copy(bower_path + 'bootstrap/fonts', 'public/fonts')
  
    .scripts([
        // Libraries
        bower_path + 'jquery/dist/jquery.min.js',
        bower_path + 'bootstrap/dist/js/bootstrap.min.js',
      
        // Vendors
        bower_path + 'bootstrap-slider/bootstrap-slider.js',
        bower_path + 'jquery.maskedinput/dist/jquery.maskedinput.js',
        bower_path + 'jquery-meiomask/dist/meiomask.js',
        bower_path + 'select2/dist/js/select2.full.js',
        bower_path + 'select2/dist/js/i18n/pt-BR.js',
        bower_path + 'raphael/raphael.min.js',
        bower_path + 'morris.js/morris.min.js',
        bower_path + 'datepicker/js/bootstrap-datepicker.js',
        bower_path + 'iCheck/icheck.js',
        bower_path + 'fastclick/lib/fastclick.js',
        bower_path + 'jquery-slimscroll/jquery.slimscroll.min.js',
        bower_path + 'bootstrap-wysihtml5/lib/js/wysihtml5-0.3.0.min.js',
        bower_path + 'moment/moment.js',
        bower_path + 'jvectormap/jquery-jvectormap.js',
        bower_path + 'jquery-confirm/jquery.confirm.js',
        
        
        // Application
        assets_path + 'javascripts/application.js',
        assets_path + 'javascripts/vendors/php2js.js',
        assets_path + 'javascripts/vendors/meiomask.js',
        assets_path + 'javascripts/vendors/datepicker.js',
        assets_path + 'javascripts/vendors/select2.init.js',
        assets_path + 'javascripts/vendors/app.js',
        assets_path + 'javascripts/modules/clients.js',
        assets_path + 'javascripts/vendors/daterangepicker/daterangepicker.js',
        assets_path + 'javascripts/vendors/knob/jquery.knob.js',
        assets_path + 'javascripts/vendors/sparkline/jquery.sparkline.js',
        
        
      ],
      'public/javascripts/application.js', './');
});
