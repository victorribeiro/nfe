<hr><h4>Informações de Substituição Tributária</h4><hr>

<div class="form-group row">
    <div class="col-sm-3">
        {!! Form::label('ncm', 'NCM*') !!}
        {!! Form::text('ncm', null, [ 'class' => 'form-control', 'placeholder' => 'NCM do Produto', 'data-mask' => 'ncm' ]) !!}
    </div>
    <div class="col-sm-3">
        {!! Form::label('cfop_id', 'CFOP*') !!}
        {!! Form::select('cfop_id', $cfops, null, [ 'class' => 'select2 form-control', 'data-placeholder' => 'Selecione o CFOP' ]) !!}
    </div>
    <div class="col-sm-3">
        {!! Form::label('tax_situation_id', 'Situação Tributária*') !!}
        {!! Form::select('tax_situation_id', $taxSituations, null, [ 'class' => 'select2 form-control', 'data-placeholder' => 'Selecione a Situação Tribitária' ]) !!}
    </div>
    <div class="col-sm-3">
        {!! Form::label('state_id', 'Estado de Destino*') !!}
        {!! Form::select('state_id', $states, null, [ 'class' => 'select2 form-control', 'data-placeholder' => 'Selecione o Estado de Destino' ]) !!}
    </div>
</div>

<div class="form-group row">
    <div class="col-sm-3">
        {!! Form::label('aliquot_icms', 'Aliquota de ICMS') !!}
        {!! Form::text('aliquot_icms', empty($product->aliquot_icms) ? null : number_format($product->aliquot_icms, 2, ',', '.'), [ 'class' => 'form-control', 'placeholder' => '100,00', 'data-mask' => 'amount' ]) !!}
    </div>
    <div class="col-sm-3">
        {!! Form::label('aliquot_icms_st', 'Aliquota de ICMS de ST') !!}
        {!! Form::text('aliquot_icms_st', empty($product->aliquot_icms_st) ? null : number_format($product->aliquot_icms_st, 2, ',', '.'), [ 'class' => 'form-control', 'placeholder' => '100,00', 'data-mask' => 'amount' ]) !!}
    </div>
    <div class="col-sm-3">
        {!! Form::label('aliquot_mva', 'Aliquota de MVA') !!}
        {!! Form::text('aliquot_mva', empty($product->aliquot_mva) ? null : number_format($product->aliquot_mva, 2, ',', '.'), [ 'class' => 'form-control', 'placeholder' => '100,00', 'data-mask' => 'amount' ]) !!}
    </div>
    <div class="col-sm-3">
        {!! Form::label('internal_aliquot_target', 'Aliquota interna do Estado de Destino') !!}
        {!! Form::text('internal_aliquot_target', empty($product->internal_aliquot_target) ? null : number_format($product->internal_aliquot_target, 2, ',', '.'), [ 'class' => 'form-control', 'placeholder' => '100,00', 'data-mask' => 'amount' ]) !!}
    </div>
    
</div>

<div class="form-group row">
    <div class="col-sm-3">
        {!! Form::label('reduction_percentage_bc', 'Percentual de redução da BC') !!}
        {!! Form::text('reduction_percentage_bc', empty($product->reduction_percentage_bc) ? null : number_format($product->reduction_percentage_bc, 2, ',', '.'), [ 'class' => 'form-control', 'placeholder' => '100,00', 'data-mask' => 'amount' ]) !!}
    </div>
    <div class="col-sm-3">
        {!! Form::label('reduction_percentage_bc_st', 'Percentual de redução da BC da ST') !!}
        {!! Form::text('reduction_percentage_bc_st', empty($product->reduction_percentage_bc_st) ? null : number_format($product->reduction_percentage_bc_st, 2, ',', '.'), [ 'class' => 'form-control', 'placeholder' => '100,00', 'data-mask' => 'amount' ]) !!}
    </div>
    <div class="col-sm-3">
        {!! Form::label('percentage_acceptance', 'Percentual de deferimento') !!}
        {!! Form::text('percentage_acceptance', empty($product->percentage_acceptance) ? null : number_format($product->percentage_acceptance, 2, ',', '.'), [ 'class' => 'form-control', 'placeholder' => '100,00', 'data-mask' => 'amount' ]) !!}
    </div>
    <div class="col-sm-3">
        {!! Form::label('credit_percentage_icms', 'Percentual de crédito de ICMS') !!}
        {!! Form::text('credit_percentage_icms', empty($product->credit_percentage_icms) ? null : number_format($product->credit_percentage_icms, 2, ',', '.'), [ 'class' => 'form-control', 'placeholder' => '100,00', 'data-mask' => 'amount' ]) !!}
    </div>
</div>

<hr><h4>Informações fiscais para PIS/COFINS</h4><hr>

<div class="form-group row">
    <div class="col-sm-3">
        {!! Form::label('aliquot_pis', 'Alíquota para PIS') !!}
        {!! Form::text('aliquot_pis', empty($product->aliquot_pis) ? null : number_format($product->aliquot_pis, 2, ',', '.'), [ 'class' => 'form-control', 'placeholder' => '100,00', 'data-mask' => 'amount' ]) !!}
    </div>
    <div class="col-sm-3">
        {!! Form::label('cst_pis_id', 'CST para PIS') !!}
        {!! Form::select('cst_pis_id', $cstPis, null, [ 'class' => 'select2 form-control', 'data-placeholder' => 'Selecione o CST para PIS' ]) !!}
    </div>
    <div class="col-sm-3">
        {!! Form::label('aliquot_cofins', 'Alíquota para COFINS') !!}
        {!! Form::text('aliquot_cofins', empty($product->aliquot_cofins) ? null : number_format($product->aliquot_cofins, 2, ',', '.'), [ 'class' => 'form-control', 'placeholder' => '100,00', 'data-mask' => 'amount' ]) !!}
    </div>
    <div class="col-sm-3">
        {!! Form::label('reduction_percentage_bc_st', 'CST para COFINS') !!}
        {!! Form::select('reduction_percentage_bc_st', $states, null, [ 'class' => 'select2 form-control', 'data-placeholder' => 'Selecione o CST para COFINS' ]) !!}
    </div>
</div>

<hr><h4>Informações fiscais para IPI</h4><hr>

<div class="form-group row">
    <div class="col-sm-3">
        {!! Form::label('aliquot_ipi', 'Alíquota para IPI') !!}
        {!! Form::text('aliquot_ipi', empty($product->aliquot_ipi) ? null : number_format($product->aliquot_ipi, 2, ',', '.'), [ 'class' => 'form-control', 'placeholder' => '100,00', 'data-mask' => 'amount' ]) !!}
    </div>
    <div class="col-sm-3">
        {!! Form::label('cst_ipi_id', 'CST para IPI') !!}
        {!! Form::select('cst_ipi_id', $cstIpi, null, [ 'class' => 'select2 form-control', 'data-placeholder' => 'Selecione o CST para IPI' ]) !!}
    </div>
</div>