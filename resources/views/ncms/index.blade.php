@extends('layouts/default')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        NCM
        <small>listagem</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/panel"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">NCM</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    @include('shared/_flash_messages')
    <div class="box-tools">
        {!! Form::open([ 'method' => 'get' ]) !!}
        <div class="input-group">
            {!! Form::text('term', $request->term, [ 'class' => 'form-control', 'placeholder' => 'Buscar por: NCM', 'autofocus' => '' ]) !!}

            <div class="input-group-btn">
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            </div>
            <div class="pull-right">
                <a href="{{ url('/panel/ncms/create') }}" class="btn btn-flat btn-primary"><i class="fa fa-plus"></i> Adicionar</a>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    
    <hr>
    @if ($ncms->isEmpty())
    <div class="alert alert-info">
        <b>Atenção!</b> Nenhum registro foi encontrado.
    </div>
    @else
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Lista de NCMs</h3>
                </div>
                <!-- /.box-header -->

                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>NCM</th>
                                <th>CFOP</th>
                                <th>Situação Tributária</th>
                                <th>Estado de Destino</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($ncms as $ncm)
                            <tr>
                                <td>{{ $ncm->ncm }}</td>
                                <td>{{ $ncm->cfop->name }}</td>
                                <td>{{ $ncm->tax_situation->code . ' - ' .  $ncm->tax_situation->name }}</td>
                                <td>{{ $ncm->state->name }}</td>
                                <td class="tooltip-code" style="min-width: 120px;">
                                    <a href="{{ url('/panel/ncms/edit', ['id' => $ncm->id]) }}" class="btn btn-primary btn-circle"
                                       data-toggle="tooltip" data-placement="top" title data-original-title="Editar">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <a href="{{ url('/panel/ncms/destroy', ['id' => $ncm->id]) }}" class="btn btn-danger btn-circle destroy"
                                       data-toggle="tooltip" data-placement="top" title data-original-title="Excluir">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-6 -->

    </div>

    <div class="text-center">
        {!! $ncms->render() !!}
    </div>
    @endif
</section>
<!-- /.content -->
@endsection