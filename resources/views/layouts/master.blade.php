<!doctype html>
<html lang="en">
<head>
    @yield('head')
    @yield('stylesheets')
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        @yield('nav')

        @yield('container')

        @yield('footer')
        <div class="control-sidebar-bg"></div>
    </div>
@yield('scripts')
@yield('javascripts')
</body>
</html>
