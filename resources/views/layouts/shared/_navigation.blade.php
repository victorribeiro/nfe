<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
<!--        <div class="user-panel">
            <div class="pull-left image">
                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
         search form 
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>-->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
<!--                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>-->
                </a>
            </li>
            <li class="treeview">
                <a href="{{ url('/panel/salesmans') }}">
                    <i class="fa fa-male"></i>
                    <span>Vendedores</span>
                </a>
            </li>
            <li class="treeview">
                <a href="{{ url('/panel/clients') }}">
                    <i class="fa fa-users"></i>
                    <span>Clientes</span>
                </a>
            </li>
            <li>
                <a href="{{ url('/panel/categories') }}">
                    <i class="fa fa-reorder"></i> <span>Categorias</span>
                </a>
            </li>
            <li class="treeview">
                <a href="{{ url('/panel/products') }}">
                    <i class="fa fa-bookmark"></i>
                    <span>Produtos</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-database"></i>
                    <span>NF-e</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-pie-chart"></i>
                    <span>Relatórios</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cogs"></i> <span>Configurações</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href=""><i class="fa fa-circle-o"></i> Importação</a></li>
                    <li><a href="{{ url('/panel/ncms') }}"><i class="fa fa-circle-o"></i> NCM</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user"></i> <span>Meus Dados</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href=""><i class="fa fa-circle-o"></i> Perfil</a></li>
                    <li><a href=""><i class="fa fa-circle-o"></i> Dados da Empresa</a></li>
                    <li><a href=""><i class="fa fa-circle-o"></i> Plano</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>