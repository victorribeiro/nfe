@extends('layouts.auth')

<!-- Main Content -->
@section('content')
<div class="login-box-body">
    <p class="login-box-msg">Esqueci a Senha</p>

    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif

    <form class="form-horizontal" role="form" method="POST" action="{{ url('/panel/password/email') }}">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="Email">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>

        <div class="row">
            <div class="col-xs-12">
                <button type="submit" class="btn btn-primary btn-block btn-flat">
                    Enviar link para restaurar a senha
                </button>
            </div>
        </div>
    </form>
    
    <a href="{{ url('/panel/login') }}" class="text-center">Já sou cadastrado</a><br />
    <a href="{{ url('/panel/register') }}" class="text-center">Ainda não sou cadastrado</a>
    
</div>
@endsection
