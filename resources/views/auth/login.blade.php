@extends('layouts.auth')

@section('content')
<div class="login-box-body">
    <p class="login-box-msg">Faça o login para iniciar a sua sessão</p>

    <form class="form-horizontal" role="form" method="POST" action="{{ url('/panel/login') }}">
        {{ csrf_field() }}
        <div class="form-group has-feedback">
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Email">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif

        </div>
        <div class="form-group has-feedback">
            <input id="password" type="password" class="form-control" name="password" required placeholder="Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif

        </div>
        <div class="row">
            <div class="col-xs-8">
                <div class="checkbox icheck">
                    <label>
                        <input type="checkbox" name="remember"> Lembrar de mim
                    </label>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
            </div>
            <!-- /.col -->
        </div>
    </form>

    <a href="{{ url('/panel/password/reset') }}">Esqueci a Senha</a><br>
    <a href="{{ url('/panel/register') }}" class="text-center">Ainda não sou cadastrado</a>

</div>
@endsection