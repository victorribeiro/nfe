<!DOCTYPE html>
<!-- saved from url=(0033)http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>/ -->
<html class=" js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths"><!--<![endif]--><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Nota Fácil</title>

        <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>

        <!-- Animate.css -->
        <link rel="stylesheet" href="{{ asset('css/site/animate.css') }}">
        <!-- Icomoon Icon Fonts-->
        <link rel="stylesheet" href="{{ asset('css/site/icomoon.css') }}">
        <!-- Simple Line Icons -->
        <link rel="stylesheet" href="{{ asset('css/site/simple-line-icons.css') }}">
        <!-- Bootstrap  -->
        <link href="{{ asset('css/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
        <!-- Owl Carousel  -->
        <link rel="stylesheet" href="{{ asset('css/site/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/site/owl.theme.default.min.css') }}">
        <!-- Style -->
        <link rel="stylesheet" href="{{ asset('css/site/style.css') }}">


        <!-- Modernizr JS -->
        <script src="{{ asset('js/site/modernizr-2.6.2.min.js') }}"></script>
        <!-- FOR IE9 below -->
        <!--[if lt IE 9]>
        <script src="js/respond.min.js"></script>
        <![endif]-->
        
        <meta name="description" content="Free HTML5 Template by FREEHTML5.CO">
        <meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive">

        <!-- Facebook and Twitter integration -->
        <meta property="og:title" content="">
        <meta property="og:image" content="">
        <meta property="og:url" content="">
        <meta property="og:site_name" content="">
        <meta property="og:description" content="">
        <meta name="twitter:title" content="">
        <meta name="twitter:image" content="">
        <meta name="twitter:url" content="">
        <meta name="twitter:card" content="">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="shortcut icon" href="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>/favicon.ico">

    </head>
    <body id="hotsite" cz-shortcut-listen="true">
        <header role="banner" id="fh5co-header">
            <div class="fluid-container">
                <nav class="navbar navbar-default">
                    <div class="navbar-header">
                        <!-- Mobile Toggle Menu Button -->
                        <a href="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>/#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#navbar">
                            <i></i>
                        </a>
                        <a class="navbar-brand" href="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>/"><span>ROTA</span>e-Social</a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="active">
                                <a href="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>/#home" data-nav-section="home"><span>Home</span></a>
                            </li>
                            <li>
                                <a href="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>/#explore" data-nav-section="explore"><span>Sobre</span></a>
                            </li>
                            <li>
                                <a href="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>/#pricing" data-nav-section="pricing"><span>Planos</span></a>
                            </li>
                            <li>
                                <a href="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>/#services" data-nav-section="services"><span>Serviços</span></a>
                            </li>
                            <li>
                                <a href="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>/#faq" data-nav-section="faq"><span>FAQ</span></a>
                            </li>
                            <li class="call-to-action">
                                <a href="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>/#" class="external" data-toggle="modal" data-target="#authentication-modal">
                                    <span>Login/Cadastre-se</span>
                                </a>
                            </li>
                        </ul>


                    </div>
                </nav>
            </div>
        </header>


        <div id="flash-message" class="container">

        </div>


        <!-- Modal -->
        <div class="modal fade" id="authentication-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                        <div class="bs-example bs-example-tabs">
                            <ul id="myTab" class="nav nav-tabs">
                                <li class="active">
                                    <a href="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>/#signin" data-toggle="tab"><i class="fa fa-fw fa-sign-in"></i> Login</a>
                                </li>
                                <li>
                                    <a href="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>/#signup" data-toggle="tab"><i class="fa fa-fw fa-user-plus"></i> Cadastre-se</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="signin">
                                <form class="new_user" role="form" method="POST" action="{{ url('/panel/login') }}">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <label for="user_email">Email</label>
                                            <input id="email" type="email" class="form-control" 
                                                   name="email" value="{{ old('email') }}" required 
                                                   autofocus autocomplete="off">
                                            
                                            @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12 form-group">
                                            <label for="user_password">Senha</label>
                                            <input id="password" type="password" class="form-control" 
                                                   name="password" required autocomplete="off">
                                            
                                            @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12 form-group">
                                            <input type="checkbox" name="remember">
                                            <label for="user_remember_me">Lembrar minha conta</label>
                                        </div>
                                    </div>

                                    <button class="btn btn-block btn-primary" type="submit">
                                        <i class="fa fa-fw fa-sign-in"></i> Entrar
                                    </button>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="signup">
                                <form class="new_user" role="form" method="POST" action="{{ url('/panel/register') }}">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-6 form-group">
                                            <label for="user_name">Nome</label>
                                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="Nome">
                                            @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>

                                        <div class="col-md-6 form-group">
                                            <label for="user_email">Email</label>
                                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="Email">
                                            @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-6 form-group">
                                            <label for="user_password">Senha</label>
                                            <input id="password" type="password" class="form-control" name="password" required placeholder="Senha">
                                            @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        </div>

                                        <div class="col-xs-6 form-group">
                                            <label for="user_password_confirmation">Confirmar senha</label>
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Confirmar Senha">
                                            @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <button class="btn btn-block btn-primary" type="submit">
                                        <i class="fa fa-fw fa-user"></i> Cadastrar conta
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <section id="fh5co-home" data-section="home" style="background-image:url(/assets/images/full_image_3.jpg)" class="animated">
            <div class="gradient"></div>
            <div class="container">
                <div class="text-wrap">
                    <div class="text-inner">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <h1 class="to-animate fadeInUp animated">ROTAeSocial</h1>
                                <h2 class="to-animate fadeInUp animated">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Id ab at deleniti similique, voluptate architecto iure commodi,
                                    repellendus tenetur placeat dolor autem sapiente alias itaque!
                                    Voluptates delectus ab quo suscipit.
                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section id="fh5co-explore" data-section="explore">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 section-heading text-center">
                        <h2 class="to-animate">Sobre</h2>
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 subtext to-animate">
                                <h3>O Rota E-Social é um serviço inovador no mercado que busca orientar seus clientes sobre os

                                    direitos e deveres do empregador e do doméstico, de forma responsável, clara, fácil e

                                    profissional, trazendo informações sobre a legislação, dicas, sugestões e uma ferramenta

                                    completa de gestão de empregado doméstico, integrada ao E-Social, que vai orientar e facilitar

                                    o pagamento mensal das obrigações legais do empregador, atendendo as exigências da PEC

                                    das domésticas aprovada em 2015.</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="fh5co-explore">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-push-5 to-animate-2">
                            <img class="img-responsive" src="./ROTA-e-Social_files/work_1.png" alt="work">
                        </div>
                        <div class="col-md-4 col-md-pull-8 to-animate-2">
                            <div class="mt">
                                <h3>COMPLETO, SIMPLES E SEGURO</h3>
                                <p>Deixe a burocracia do seu empregado domestico com a RotaE-social! Tudo que você precisa em um só lugar e integrado com o E-Social. Gere folha de salários, 13º, férias, impostos e muito mais.</p>
                                <ul class="list-nav">
                                    <li><i class="icon-check2"></i> Tudo dentro da Lei</li>
                                    <li><i class="icon-check2"></i> Admissão e Recisão</li>
                                    <li><i class="icon-check2"></i> Folha de Salários, 13º e Férias.</li>
                                    <li><i class="icon-check2"></i> Controle de Ponto</li>
                                    <li><i class="icon-check2"></i> Receba alertas e orientações por e-mail.</li>
                                    <li><i class="icon-check2"></i> Emissão da Guia do E-Social</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>



        <div class="getting-started getting-started-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 to-animate">
                        <h3>DEIXE A BUROCRACIA CONOSCO!</h3>
                        <p>Temos planos a partir de R$ 0,13 por dia!!</p>
                    </div>
                    <div class="col-md-6 to-animate-2">
                        <div class="call-to-action text-right">
                            <a href="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>/#" class="sign-up" data-toggle="modal" data-target="#authentication-modal">Teste Grátis</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <section id="fh5co-pricing" data-section="pricing">
            <div class="fh5co-pricing">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 section-heading text-center">
                            <h2 class="to-animate">Planos</h2>
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2 subtext">
                                    <h3 class="to-animate">
                                        Sem limite de empregados, simples, fácil, seguro e dentro da lei!
                                        Escolha o seu plano e deixe a burocracia conosco.
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="pricing">
                            <div class="col-md-3">
                                <div class="price-box to-animate-2">
                                    <h2 class="pricing-plan">Basic</h2>
                                    <div class="price"><sup class="currency">R$</sup>14,90<small>/mês</small></div>
                                    <p>
                                        Modulo NFE<br>
                                        Modulo Clientes
                                        <br>
                                        <br>
                                        <br>
                                        Até 30 Notas Fiscais p/ Mês<br>
                                        Suporte Via Email<br>
                                        Relatórios<br>
                                        Apenas 1 usuário<br><br>
                                        
                                        <small>* Usuário Extra acréscimo de R$ 5,00 por mês</small>
                                        
                                    </p>
                                    <a href="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>/#" class="btn btn-select-plan btn-sm">Cadastre-se</a>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="price-box to-animate-2">
                                    <h2 class="pricing-plan">Pro</h2>
                                    <div class="price"><sup class="currency">R$</sup>19,90<small>/mês</small></div>
                                    <p>
                                        Modulo NFE<br>
                                        Modulo Clientes
                                        Modulo Estoque
                                        <br>
                                        <br>
                                        Até 60 Notas Fiscais p/ Mês<br>
                                        Suporte Via Email<br>
                                        Relatórios<br>
                                        Apenas 2 usuário<br><br>
                                        
                                        <small>* Usuário Extra acréscimo de R$ 5,00 por mês</small>
                                    </p>
                                    <a href="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>/#" class="btn btn-select-plan btn-sm">Cadastre-se</a>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="price-box to-animate-2">
                                    <h2 class="pricing-plan">Maxi</h2>
                                    <div class="price"><sup class="currency">R$</sup>29,90<small>/mês</small></div>
                                    <p>
                                        Modulo NFE<br>
                                        Modulo Clientes
                                        Modulo Estoque
                                        <br>
                                        <br>
                                        Até 100 Notas Fiscais p/ Mês<br>
                                        Suporte Via Email<br>
                                        Relatórios<br>
                                        Apenas 3 usuário<br><br>
                                        
                                        <small>* Usuário Extra acréscimo de R$ 5,00 por mês</small>
                                    </p>
                                    <a href="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>/#" class="btn btn-select-plan btn-sm">Cadastre-se</a>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="price-box to-animate-2">
                                    <h2 class="pricing-plan">Ultra</h2>
                                    <div class="price"><sup class="currency">R$</sup>69,90<small>/mês</small></div>
                                    <p>
                                        Modulo NFE<br>
                                        Modulo Clientes
                                        Modulo Estoque
                                        <br>
                                        <br>
                                        Ilimitado Notas Fiscais p/ Mês<br>
                                        Suporte Via Email<br>
                                        Relatórios<br>
                                        Apenas 5 usuário<br><br>
                                        
                                        <small>* Usuário Extra acréscimo de R$ 5,00 por mês</small>
                                    </p>
                                    <a href="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>/#" class="btn btn-select-plan btn-sm">Cadastre-se</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 to-animate">
                          <!-- <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. <a href="#">Learn More</a></p> -->
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section id="fh5co-services" data-section="services">
            <div class="fh5co-services">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 section-heading text-center">
                            <h2 class="to-animate">Serviços</h2>
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2 subtext">
                                    <h3 class="to-animate">
                                        COMPLETO, SIMPLES E SEGURO. <br>
                                        Conheça alguns de nossos serviços e deixe a RotaE-Social simplificar a sua vida!
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="box-services">
                                <i class="icon-chemistry to-animate-2"></i>
                                <div class="fh5co-post to-animate">
                                    <h3>RECIBOS DIVERSOS</h3>
                                    <p>
                                        Em nosso site você gera e imprime os recibos mensais de salários e
                                        também os do 13º e das Férias do seu empregado doméstico
                                    </p>
                                </div>
                            </div>

                            <div class="box-services">
                                <i class="icon-energy to-animate-2"></i>
                                <div class="fh5co-post to-animate">
                                    <h3>ADMISSÃO, RESCISÃO E LICENSA.</h3>
                                    <p>
                                        Completo: Da admissão à rescisão. Orientamos preenchimento de carteira de trabalho,
                                        emissão e cálculo de aviso prévio, pedido de dispensa e cálculos trabalhistas.
                                        Orientações para Auxilio doença e licença maternidade.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="box-services">
                                <i class="icon-trophy to-animate-2"></i>
                                <div class="fh5co-post to-animate">
                                    <h3>ORIENTAÇÕES E ALERTAS POR E-MAIL</h3>
                                    <p>
                                        Emitimos alertas por e-mail para você não perder nenhum prazos,
                                        nem pagar juros e multas.
                                    </p>
                                </div>
                            </div>

                            <div class="box-services">
                                <i class="icon-paper-plane to-animate-2"></i>
                                <div class="fh5co-post to-animate">
                                    <h3>CONTROLE DE PONTO</h3>
                                    <p>
                                        Independente da jornada de trabalho, calculamos suas horas
                                        extras, atrasos e adicionais.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="box-services">
                                <i class="icon-people to-animate-2"></i>
                                <div class="fh5co-post to-animate">
                                    <h3>EMISSÃO DE GUIA DO E-SOCIAL</h3>
                                    <p>
                                        Integrado ao site do E-social, emitimos e enviamos as guias mensais
                                        de recolhimento de tributos com valores e data de vencimento.
                                    </p>
                                </div>
                            </div>

                            <div class="box-services">
                                <i class="icon-screen-desktop to-animate-2"></i>
                                <div class="fh5co-post to-animate">
                                    <h3>SUPORTE ESPECIALIZADO</h3>
                                    <p>
                                        Suporte especializado para orientações trabalhistas e duvidas do nosso sistema.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="call-to-action text-center to-animate"><a href="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>/#" class="btn btn-learn">Learn More</a></div>
                </div>
            </div>
        </section>



        <section id="fh5co-faq" data-section="faq">
            <div class="fh5co-faq">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 section-heading text-center">
                            <h2 class="to-animate">Dúvidas Frequentes</h2>
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2 subtext">
                                    <h3 class="to-animate">
                                        Confira nossas dúvidas mais frequentes! Caso sua resposta não esteja aqui, confira o nosso
                                        blog onde temos diversas outras duvidas solucionadas e organizadas por temas e categorias.
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="box-faq to-animate-2">
                                <h3>1. O que é o eSocial?</h3>
                                <p>
                                    O eSocial é um sistema de escrituração digital das obrigações fiscais, previdenciárias e
                                    trabalhistas, é um projeto do Governo Federal que vai unificar a prestação de informações
                                    pelo empregador em relação aos seus empregados (como cadastramento, vínculos,
                                    contribuições previdenciárias e folha de pagamento, entre outros), gerido pela CEF, INSS,
                                    Ministério do Trabalho e da Previdência Social e Receita Federal do Brasil.
                                </p>
                            </div>

                            <div class="box-faq to-animate-2">
                                <h3>2. Como funciona o eSocial para o empregador doméstico?</h3>
                                <p>
                                    O eSocial para o empregador doméstico é uma solução web para prestação de informação
                                    simplificada e online por meio do endereço www.eSocial.gov.br . A obrigatoriedade de uso do
                                    eSocial observa o previsto na Lei Complementar 150/2015.
                                </p>
                            </div>

                            <div class="box-faq to-animate-2">
                                <h3>3. Qual é a jornada de trabalho fixada em lei para a doméstica?</h3>
                                <p>
                                    A jornada diária do empregado doméstico&nbsp;definida por lei é de oito horas diárias ou 44 horas
                                    semanais, podendo ser reduzida. A remuneração deverá ser igual ou maior ao salário mínimo,
                                    que poderá variar por estado. No caso de jornada reduzida, o valor do salário poderá ser
                                    proporcional, existindo também algumas diferenças, quanto aos dias de férias, por exemplo.
                                </p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="box-faq to-animate-2">
                                <h3>4. O empregado doméstico tem direito a férias remuneradas?</h3>
                                <p>
                                    Todo empregado doméstico tem direito a férias remuneradas. O empregador deve dar o aviso
                                    de férias&nbsp;ao empregado com antecedência de 30 dias. Este comunicado deve ser feito por
                                    escrito&nbsp;e&nbsp;assinado pelo doméstico.
                                </p>
                            </div>

                            <div class="box-faq to-animate-2">
                                <h3>5. O que fazer na demissão da doméstica?</h3>
                                <p>
                                    No caso de demissão da empregada doméstica, é necessário dar o aviso prévio de 30 dias ao
                                    empregado que conte com até 1 ano de serviço para o mesmo empregador.
                                    Deverão ser acrescentados 3 dias para cada ano trabalhado para este mesmo empregador, até
                                    no máximo de 60 dias. O limite total de aviso prévio da empregada doméstica é de 90 dias.
                                </p>
                            </div>

                            <div class="box-faq to-animate-2">
                                <h3>6. Doméstica tem direito ao FGTS?</h3>
                                <p>
                                    Com a nova PEC das Domésticas, todos os trabalhadores considerados domésticos tem direito
                                    ao FGTS, o fundo de garantia. Todo mês, o empregador deverá recolher através do Simples
                                    Doméstico, na guia do eSocial, o FGTS do doméstico. O valor recolhido para o FGTS é de 8% do
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- <hr /> -->

        <div class="getting-started getting-started-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 to-animate">
                        <h3>DEIXE A BUROCRACIA CONOSCO!</h3>
                        <p>Temos planos a partir de R$ 0,13 por dia!!</p>
                    </div>
                    <div class="col-md-6 to-animate-2">
                        <div class="call-to-action text-right">
                            <a href="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>/#" class="sign-up" data-toggle="modal" data-target="#authentication-modal">Teste Grátis</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div id="fh5co-footer" role="contentinfo">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 to-animate">
                        <h3 class="section-title">Sobre nós</h3>
                        <p>
                            Acompanhe as nossas redes sociais e receba orientações, tire duvidas e
                            fique por dentro dasnovidades sobre a PEC das Domésticas.
                        </p>

                        <h3 class="section-title">Connect with Us</h3>
                        <ul class="social-media">
                            <li><a href="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>/#" class="facebook"><i class="icon-facebook"></i></a></li>
                            <li><a href="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>/#" class="twitter"><i class="icon-twitter"></i></a></li>
                            <li><a href="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>/#" class="dribbble"><i class="icon-dribbble"></i></a></li>
                            <li><a href="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>/#" class="github"><i class="icon-github-alt"></i></a></li>
                        </ul>

                        <h3 class="section-title">Localização</h3>
                        <ul class="contact-info">
                            <li><i class="icon-map-marker"></i>Rua Pio IX, nº 435, Bloco B, Sala 1204, Torre, Recife – PE</li>
                            <!--
                            <li><i class="icon-phone"></i>+ 1235 2355 98</li>
                            <li><i class="icon-envelope"></i><a href="#">info@yoursite.com</a></li>
                            <li><i class="icon-globe2"></i><a href="#">www.yoursite.com</a></li>
                            -->
                        </ul>
                    </div>

                    <div class="col-md-offset-2 col-md-6 to-animate">
                        <h3 class="section-title">Deixe sua mensagem</h3>
                        <form class="contact-form">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name" class="sr-only">Name</label>
                                        <input type="name" class="form-control" id="name" placeholder="Name" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg==&quot;); background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; background-repeat: no-repeat;">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email" class="sr-only">Email</label>
                                        <input type="email" class="form-control" id="email" placeholder="Email">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="subject" class="sr-only">Assunto</label>
                                <input type="subject" class="form-control" id="subject" placeholder="Assunto">
                            </div>

                            <div class="form-group">
                                <label for="message" class="sr-only">Message</label>
                                <textarea class="form-control" id="message" rows="7" placeholder="Message"></textarea>
                            </div>
                            <div class="form-group">
                                <input type="submit" id="btn-submit" class="btn btn-block btn-send-message btn-md" value="Send Message">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <script src="{{ asset('js/site/jquery.min.js') }}"></script>
        <script src="{{ asset('js/site/jquery.easing.1.3.js') }}"></script>
        <script src="{{ asset('js/site/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/site/jquery.waypoints.min.js') }}"></script>
        <script src="{{ asset('js/site/jquery.stellar.min.js') }}"></script>
        <script src="{{ asset('js/site/owl.carousel.min.js') }}"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>
        <script src="{{ asset('js/site/google_map.js') }}"></script>
        <script src="{{ asset('js/site/main.js') }}"></script>
        
    </body>
</html>
