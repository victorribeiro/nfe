@extends('layouts/default')

@section('content')
    <section class="content-header">
        <h1>
            Produtos
            <small>Editar Produto</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/panel"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/panel/products"><i class="fa fa-bookmark"></i>Produtos</a></li>
            <li class="active">Editar</li>
        </ol>
    </section>

    <section class="content">
        @include('shared/_flash_messages')
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-group"></i> Editar Produto</h3>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            {!! Form::model($product, ['url' => ['/panel/products/update', $product->id], 'method' => 'put', 'files' => true]) !!}

                            @include('products._form')

                            <div class="form-group">
                                <i class=""></i>
                                {!! Form::submit('Salvar', ['class' => 'btn btn-primary btn-block']) !!}
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection