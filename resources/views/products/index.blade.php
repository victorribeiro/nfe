@extends('layouts/default')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Produtos
        <small>listagem</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/panel"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Produtos</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    @include('shared/_flash_messages')
    <div class="box-tools">
        {!! Form::open([ 'method' => 'get' ]) !!}
        <div class="input-group">
            {!! Form::text('term', $request->term, [ 'class' => 'form-control', 'placeholder' => 'Buscar por: Nome', 'autofocus' => '' ]) !!}

            <div class="input-group-btn">
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            </div>
            <div class="pull-right">
                <a href="{{ url('/panel/products/create') }}" class="btn btn-flat btn-primary"><i class="fa fa-plus"></i> Adicionar</a>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    
    <hr>
    @if ($products->isEmpty())
    <div class="alert alert-info">
        <b>Atenção!</b> Nenhum registro foi encontrado.
    </div>
    @else
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Lista de Produtos</h3>
                </div>
                <!-- /.box-header -->

                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Categoria</th>
                                <th>NCM</th>
                                <th>Preço (R$)</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($products as $product)
                            <tr>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->category->name }}</td>
                                <td>{{ $product->ncm }}</td>
                                <td>{{ $product->value }}</td>
                                <td class="tooltip-code">
                                    <a href="{{ url('/panel/products/edit', ['id' => $product->id]) }}" class="btn btn-primary btn-circle"
                                       data-toggle="tooltip" data-placement="top" title data-original-title="Editar">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <a href="{{ url('/panel/products/destroy', ['id' => $product->id]) }}" class="btn btn-danger btn-circle destroy"
                                       data-toggle="tooltip" data-placement="top" title data-original-title="Excluir">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-6 -->

    </div>

    <div class="text-center">
        {!! $products->render() !!}
    </div>
    @endif
</section>
<!-- /.content -->
@endsection