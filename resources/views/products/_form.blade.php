<div class="form-group row">
    <div class="col-sm-3">
        {!! Form::label('code', 'Código*') !!}
        {!! Form::text('code', null, [ 'class' => 'form-control', 'placeholder' => 'Código do Produto' ]) !!}
    </div>
    <div class="col-sm-9">
        {!! Form::label('name', 'Nome ou descrição*') !!}
        {!! Form::text('name', null, [ 'class' => 'form-control', 'placeholder' => 'Nome ou descrição do Produto' ]) !!}
    </div>
</div>

<div class="form-group row">
    <div class="col-sm-4">
        {!! Form::label('category_id', 'Categoria*') !!}
        {!! Form::select('category_id', $categories, null, [ 'class' => 'select2 form-control', 'data-placeholder' => 'Selecione a categoria' ]) !!}
    </div>
    <div class="col-sm-4">
        {!! Form::label('unit_measure_id', 'Unidade de Medida*') !!}
        {!! Form::select('unit_measure_id', $unitMeasures, null, [ 'class' => 'select2 form-control', 'data-placeholder' => 'Selecione a unidade de medida' ]) !!}
    </div>
</div>

<div class="form-group row">
    <div class="col-sm-4">
        {!! Form::label('ncm', 'NCM*') !!}
        {!! Form::text('ncm', null, [ 'class' => 'form-control', 'placeholder' => 'NCM do Produto', 'data-mask' => 'ncm' ]) !!}
    </div>
    <div class="col-sm-4">
        {!! Form::label('value', 'Valor (R$)*') !!}
        {!! Form::text('value', empty($product->value) ? null : number_format($product->value, 2, ',', '.'), [ 'class' => 'form-control', 'placeholder' => '100,00', 'data-mask' => 'amount' ]) !!}
    </div>
</div>

<div class="form-group row">
    <div class="col-sm-6">
        {!! Form::label('cfop_id', 'CFOP') !!}
        {!! Form::select('cfop_id', $cfops, null, [ 'class' => 'select2 form-control', 'data-placeholder' => 'Selecione o CFOP Preferencial' ]) !!}
    </div>
    <div class="col-sm-6">
        {!! Form::label('tax_situation_id', 'Código da situação tributária do produto (CST)') !!}
        {!! Form::select('tax_situation_id', $taxSituations, null, [ 'class' => 'select2 form-control', 'data-placeholder' => 'Selecione o CST' ]) !!}
    </div>
</div>

<div class="form-group row">
    <div class="col-xs-4">
        {!! Form::label('code_anp', 'Código ANP') !!}
        {!! Form::text('code_anp', null, [ 'class' => 'form-control', 'placeholder' => 'Código ANP' ]) !!}
    </div>
    <div class="col-xs-8">
        {!! Form::label('product_origin_id', 'Código ANP') !!}
        {!! Form::select('product_origin_id', $productOrigins, null, [ 'class' => 'select2 form-control', 'data-placeholder' => 'Selecione a origem do Produto' ]) !!}
    </div>
</div>

<div class="form-group row">
    <div class="col-sm-4">
        {!! Form::label('image', 'Imagem') !!}
        {!! Form::file('image', null, [ 'class' => 'form-control', 'placeholder' => 'Selecione a Imagem' ]) !!}
    </div>
</div>


