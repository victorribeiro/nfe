<div class="form-group row">
    <div class="col-lg-12">
        {!! Form::label('name', 'Nome *') !!}
        <div class="input-group">
            <span class="input-group-addon">
              <i class="fa fa-user"></i>
            </span>
            {!! Form::text('name', null, [ 'class' => 'form-control', 'placeholder' => 'Nome da Categoria' ]) !!}
        </div>
    </div>
</div>