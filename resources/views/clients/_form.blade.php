<hr><h4>Dados Gerais</h4><hr>
<div class="form-group row">
    <div class="col-md-3">
        {!! Form::label('kind_person', 'Tipo de pessoa *') !!}
        <div class="input-group">
            {!! Form::select('kind_person', [ 'pj' => 'Pessoa Jurídica', 'pf' => 'Pessoa Física' ], null, [ 'id' => 'kind_person', 'class' => 'form-control' ]) !!}
        </div>
    </div>
</div>

<div class="form-group row">
    <div class="col-lg-12">
        {!! Form::label('company_name', 'Nome/Razão Social *') !!}
        <div class="input-group">
            <span class="input-group-addon">
              <i class="fa fa-user"></i>
            </span>
            {!! Form::text('company_name', null, [ 'class' => 'form-control', 'placeholder' => 'Nome ou Razão Social do Cliente' ]) !!}
        </div>
    </div>
</div>

<div class="form-group row pj">
    <div class="col-lg-12">
        {!! Form::label('fantasy_name', 'Nome Fantasia') !!}
        <div class="input-group">
            <span class="input-group-addon">
              <i class="fa fa-user"></i>
            </span>
            {!! Form::text('fantasy_name', null, [ 'class' => 'form-control', 'placeholder' => 'Nome Fantasia' ]) !!}
        </div>
    </div>
</div>

<div class="form-group row">
    <div class="col-sm-4">
        {!! Form::label('document', 'CPF/CNPJ *') !!}
        <div id='input-document'>
            {!! Form::text('document', null, [ 'id' => 'document', 'class' => 'form-control', 'placeholder' => 'CPF/CNPJ', 'data-mask' => 'cnpj' ]) !!}
        </div>
    </div>
<!--    <div class="col-sm-4 pf">
        {!! Form::label('document', 'CPF *') !!}
        {!! Form::text('document', null, [ 'id' => 'document2', 'class' => 'form-control', 'placeholder' => 'CPF', 'data-mask' => 'cpf' ]) !!}
    </div>-->
    <div class="col-sm-4 pj">    
        {!! Form::label('state_registration', 'Inscrição Estadual') !!}
        {!! Form::text('state_registration', null, [ 'class' => 'form-control', 'placeholder' => 'Inscrição Estadual' ]) !!}
    </div>
    <div class="col-sm-4 pj">
        {!! Form::label('municipal_registration', 'Inscrição Municipal') !!}
        {!! Form::text('municipal_registration', null, [ 'class' => 'form-control', 'placeholder' => 'Inscrição Municipal' ]) !!}
    </div>
</div>

<hr><h4>Contato</h4><hr>

<div class="form-group row pj">
    <div class="col-xs-12">
        {!! Form::label('contact_name', 'Nome') !!}
        <div class="input-group">
            <span class="input-group-addon">
              <i class="fa fa-user"></i>
            </span>
            {!! Form::text('contact_name', null, [ 'class' => 'form-control', 'placeholder' => 'Nome do Contato' ]) !!}
        </div>
    </div>
</div>

<div class="form-group row">
    <div class="col-sm-6">
        {!! Form::label('contact_email', 'Email') !!}
        <div class="input-group">
            <span class="input-group-addon">
              <i class="fa fa-envelope"></i>
            </span>
            {!! Form::text('contact_email', null, [ 'class' => 'form-control', 'placeholder' => 'Email do Contato' ]) !!}
        </div>
    </div>
    <div class="col-sm-4">
        {!! Form::label('contact_phone', 'Telefone') !!}
        {!! Form::text('contact_phone', null, [ 'class' => 'form-control', 'placeholder' => 'Telefone do Contato', 'data-mask' => 'phone-br' ]) !!}
    </div>
</div>

<hr><h4>Endereço</h4><hr>

<div class="form-group">
    <div class="row">
        <div class="col-sm-3">
            {!! Form::label('cep', 'CEP') !!}
            {!! Form::text('cep', null, [ 'id' => 'address-cep', 'data-prefix' => 'address', 'class' => 'form-control', 'placeholder' => 'Digite o CEP', 'data-mask' => 'cep' ]) !!}
        </div>

        <div class="col-sm-12">
            <span id="correios-message" class="text-danger"></span>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col-sm-9">
            {!! Form::label('street', 'Endereço') !!}
            {!! Form::text('street', null, [ 'id' => 'address-street', 'class' => 'form-control', 'placeholder' => 'Rua, Av, etc...' ]) !!}
        </div>

        <div class="col-sm-3">
            {!! Form::label('number', 'Número') !!}
            {!! Form::text('number', null, [ 'class' => 'form-control', 'placeholder' => '999' ]) !!}
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col-sm-4">
            {!! Form::label('complement', 'Complemento') !!}
            {!! Form::textarea('complement', null, [ 'class' => 'form-control', 'rows' => 2, 'placeholder' => 'Apartamento, Comercial (Opcional)' ]) !!}
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col-sm-3">
            {!! Form::label('neighborhood', 'Bairro') !!}
            {!! Form::text('neighborhood', null, [ 'id' => 'address-neighborhood', 'class' => 'form-control', 'placeholder' => 'Ex:. Boa Viagem' ]) !!}
        </div>

        <div class="col-sm-4">
            {!! Form::label('city', 'Cidade') !!}
            {!! Form::text('city', null, [ 'id' => 'address-city', 'class' => 'form-control', 'placeholder' => 'Ex:. Recife' ]) !!}
        </div>

        <div class="col-sm-2">
            {!! Form::label('state', 'Estado') !!}
            {!! Form::text('state', null, [ 'id' => 'address-state', 'class' => 'form-control', 'placeholder' => 'Ex:. PE' ]) !!}
        </div>
        
        <div class="col-sm-3">
            {!! Form::label('countrie', 'País') !!}
            {!! Form::select('countrie', $countries, null, [ 'class' => 'select2 form-control', 'data-placeholder' => 'Selecione o país' ]) !!}
        </div>
    </div>
</div>
