@extends('layouts/default')

@section('content')
    <section class="content-header">
        <h1>
            Vendedores
            <small>Cadastrar novo Vendedor</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/panel"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/panel/salesmans"><i class="fa fa-male"></i>Vendedores</a></li>
            <li class="active">Novo</li>
        </ol>
    </section>

    <section class="content">
        @include('shared/_flash_messages')
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-group"></i> Cadastrar novo Vendedor</h3>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            {!! Form::open(['url' => '/panel/salesmans/store', 'method' => 'post']) !!}

                            @include('salesmans._form')

                            <div class="form-group">
                                <i class=""></i>
                                {!! Form::submit('Criar Vendedor', ['class' => 'btn btn-primary btn-block']) !!}
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection