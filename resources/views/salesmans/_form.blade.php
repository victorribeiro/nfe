<hr><h4>Dados Gerais</h4><hr>
<div class="form-group row">
    <div class="col-lg-12">
        {!! Form::label('name', 'Nome*') !!}
        <div class="input-group">
            <span class="input-group-addon">
              <i class="fa fa-user"></i>
            </span>
            {!! Form::text('name', null, [ 'class' => 'form-control', 'placeholder' => 'Nome do vendedor' ]) !!}
        </div>
    </div>
</div>

<div class="form-group row">
    <div class="col-sm-4">
        {!! Form::label('document', 'CPF/*') !!}
        <div id='input-document'>
            {!! Form::text('document', null, [ 'id' => 'document', 'class' => 'form-control', 'placeholder' => 'CPF do vendedor', 'data-mask' => 'cpf' ]) !!}
        </div>
    </div>
</div>

<hr><h4>Contato</h4><hr>

<div class="form-group row">
    <div class="col-sm-6">
        {!! Form::label('email', 'Email') !!}
        <div class="input-group">
            <span class="input-group-addon">
              <i class="fa fa-envelope"></i>
            </span>
            {!! Form::text('email', null, [ 'class' => 'form-control', 'placeholder' => 'Email do vendedor' ]) !!}
        </div>
    </div>
    <div class="col-sm-4">
        {!! Form::label('phone', 'Telefone') !!}
        {!! Form::text('phone', null, [ 'class' => 'form-control', 'placeholder' => 'Telefone do vendedor', 'data-mask' => 'phone-br' ]) !!}
    </div>
</div>

<hr><h4>Endereço</h4><hr>

<div class="form-group">
    <div class="row">
        <div class="col-sm-3">
            {!! Form::label('cep', 'CEP') !!}
            {!! Form::text('cep', null, [ 'id' => 'address-cep', 'data-prefix' => 'address', 'class' => 'form-control', 'placeholder' => 'Digite o CEP', 'data-mask' => 'cep' ]) !!}
        </div>

        <div class="col-sm-12">
            <span id="correios-message" class="text-danger"></span>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col-sm-9">
            {!! Form::label('street', 'Endereço') !!}
            {!! Form::text('street', null, [ 'id' => 'address-street', 'class' => 'form-control', 'placeholder' => 'Rua, Av, etc...' ]) !!}
        </div>

        <div class="col-sm-3">
            {!! Form::label('number', 'Número') !!}
            {!! Form::text('number', null, [ 'class' => 'form-control', 'placeholder' => '999' ]) !!}
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col-sm-4">
            {!! Form::label('complement', 'Complemento') !!}
            {!! Form::textarea('complement', null, [ 'class' => 'form-control', 'rows' => 2, 'placeholder' => 'Apartamento, Comercial (Opcional)' ]) !!}
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col-sm-3">
            {!! Form::label('neighborhood', 'Bairro') !!}
            {!! Form::text('neighborhood', null, [ 'id' => 'address-neighborhood', 'class' => 'form-control', 'placeholder' => 'Ex:. Boa Viagem' ]) !!}
        </div>

        <div class="col-sm-4">
            {!! Form::label('city', 'Cidade') !!}
            {!! Form::text('city', null, [ 'id' => 'address-city', 'class' => 'form-control', 'placeholder' => 'Ex:. Recife' ]) !!}
        </div>

        <div class="col-sm-2">
            {!! Form::label('state', 'Estado') !!}
            {!! Form::text('state', null, [ 'id' => 'address-state', 'class' => 'form-control', 'placeholder' => 'Ex:. PE' ]) !!}
        </div>
        
        <div class="col-sm-3">
            {!! Form::label('countrie', 'País') !!}
            {!! Form::select('countrie', $countries, null, [ 'class' => 'select2 form-control', 'data-placeholder' => 'Selecione o país' ]) !!}
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col-sm-4">
            {!! Form::label('note', 'Observação') !!}
            {!! Form::textarea('note', null, [ 'class' => 'form-control', 'rows' => 2, 'placeholder' => 'Observações' ]) !!}
        </div>
    </div>
</div>