var Application = {
  init: function () {
    this.confirmation();
    this.tooltip();
    this.get_address();
  },

  confirmation: function () {
    $('.destroy').confirm({
      text: "Você deseja realmente excluir esse registro?",
      title: "Requer confirmação",
      confirmButton: "Sim",
      cancelButton: "Não",
      get: true,
      confirmButtonClass: "btn-danger",
      cancelButtonClass: "btn-default",
      dialogClass: "modal-dialog" // Bootstrap classes for large modal
    });
  },

  tooltip: function () {
    $('.tooltip-code').tooltip({
      selector: "[data-toggle=tooltip]",
      container: "body"
    })

    // popover demo
    $("[data-toggle=popover]")
      .popover()
  },

  get_address: function () {
    $("#address-cep").change(function() {
      var cep = $(this).val();

      $.get("http://apps.widenet.com.br/busca-cep/api/cep.json", { code: cep },
        function (response) {
          if( response.status != 1 ){
            alert(response.message || "Houve um erro desconhecido");
            return;
          }
          $("#address-street").val( response.address );
          $("#address-neighborhood").val( response.district );
          $("#address-city").val( response.city );
          $("#address-state").val( response.state );
        }
      );
    });
  },
};

$(document).ready(function () { Application.init(); });