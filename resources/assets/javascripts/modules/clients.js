var Clients = {
    init: function() {
        this.kind_person();
    },
    
    kind_person: function() {
        $("#kind_person").on('change', function(){
            value = $(this).val();
            if (value == 'pj') {
                $('.pf').hide();
                $('.pj').show();
                $('#document').remove();
                $('#input-document').append('<input id="document" class="form-control" placeholder="CPF/CNPJ" name="document" type="text" data-mask="cnpj">');
            } else {
                $('.pj').hide();
                $('.pf').show();
                $('#document').remove();
                $('#input-document').append('<input id="document" class="form-control" placeholder="CPF/CNPJ" name="document" type="text" data-mask="cpf">');
            }
            MeioMask.simple();
            MeioMask.extras();
        });
    }
};

$(document).ready(function () {
    Clients.init();
});