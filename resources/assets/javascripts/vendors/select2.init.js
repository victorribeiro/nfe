var Select2 = {
  init: function () {
    this.simple();
    this.remote();
  },

  simple: function () {
    $('.select2').not('.multi-select, .select-multiple, .phpdebugbar select, .no-select2').select2();
  },

  remote: function() {
    $('.select2-remote').select2({
      delay: 250,
      width: '100%',
      minimumInputLength: 1,
      ajax: {
        dataType: 'json',
        url: function() {
          return "/select2/" + ($(this).data('search-by'));
        },
        data: function (params) {
          return {
            term: params.term, // search term
            page: params.page
          };
        },
        processResults: function (data, params) {
          // parse the results into the format expected by Select2
          // since we are using custom formatting functions we do not need to
          // alter the remote JSON data, except to indicate that infinite
          // scrolling can be used
          params.page = params.page || 1;

          return {
            results: data.options,
            pagination: {
              more: (params.page * 30) < data.total_count
            }
          };
        },
        cache: true
      },

      initSelection: function(element, callback) {
        var current_id = $(element).data('id');

        if (current_id !== '') {
          return $.ajax({
            dataType: 'json',
            url: "/select2/" + ($(element).data('search-by')),
            data: {
              id: current_id
            },
            success: function(response) {
              return callback(response.options);
            }
          });
        }
      }
    });
  }
};

$(document).ready(function () {
  $.fn.select2.defaults.set('language', 'pt-BR');
  Select2.init();

  console.log($.fn.select2.defaults);
});
