var Fileinput = {
  init: function () {
    this.simple();
  },

  simple: function () {
    // { showUpload: false }
    $('input:file').fileinput();
  }
};

$(document).on('ready', function () { Fileinput.init(); });
