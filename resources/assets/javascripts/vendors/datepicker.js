var Datepicker = {
  init: function () {
    this.simple();
  },

  simple: function () {
    $('.bs-datepicker').datepicker({
      format: "dd/mm/yyyy",
      language: "pt-BR"
    });
  }
};

$(document).ready(function () { Datepicker.init(); });
