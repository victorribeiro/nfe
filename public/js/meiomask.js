var MeioMask = {
  init: function () {
    this.extras();
    this.simple();
  },

  simple: function () {
    $('input[data-mask]').each(function() {
      var input = $(this);
      input.setMask( input.data('mask') );
    });
  },

  extras: function () {
    $.mask.rules.n = /[0-9]?/

    $.mask.masks['phone-br'] = { mask : '(99) 99999-9999' };
    $.mask.masks['phone-br2'] = { mask : '(99) 9999-9999' };
    $.mask.masks['percent'] = { mask : '199' };
    $.mask.masks['int'] = { mask : '999999999999' };
    $.mask.masks['float'] = { mask : '99.9999999999', type: 'reverse' };
    $.mask.masks['decimal-us'] = { mask : '99.999,999,999,999', type : 'reverse' };
    $.mask.masks['amount'] = { mask : '99,999.999.999.999', type : 'reverse' };
    $.mask.masks['cep'] = { mask : '99999-999' };
    $.mask.masks['date'] = { mask : '99/99/9999' };
    $.mask.masks['cpf'] = { mask : '999.999.999-99' };
  }
};

$(document).on('ready', function () { MeioMask.init(); });
