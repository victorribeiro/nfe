<?php

namespace App\Repositories;

use App\Models\Client;

/**
 * Description of ClientRepository
 *
 * @author victor
 */
class ClientRepository extends BaseRepository
{
    public $name = 'Cliente';
    public $gender = 'o';

    public function __construct(Client $eloquent)
    {
        $this->eloquent = $eloquent;
    }
    
    public function beforeStore($attributes)
    {
        $this->eloquent->company_id = \Auth::user()->company_id;

        return $attributes;
    }
}
