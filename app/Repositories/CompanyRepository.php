<?php

namespace App\Repositories;

use App\Models\Company;

class CompanyRepository extends BaseRepository
{
    public $name = 'Empresa';
    public $gender = 'a';

    public function __construct(Company $eloquent)
    {
        $this->eloquent = $eloquent;
    }
}
