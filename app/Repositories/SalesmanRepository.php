<?php

namespace App\Repositories;

use App\Models\Salesman;

/**
 * Description of SalesmanRepository
 *
 * @author victor
 */
class SalesmanRepository extends BaseRepository
{
    public $name = 'Vendedor';
    public $gender = 'o';

    public function __construct(Salesman $eloquent)
    {
        $this->eloquent = $eloquent;
    }
    
    public function beforeStore($attributes)
    {
        $this->eloquent->company_id = \Auth::user()->company_id;

        return $attributes;
    }
}
