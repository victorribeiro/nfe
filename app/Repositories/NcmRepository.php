<?php

namespace App\Repositories;

use App\Models\Ncm;

/**
 * Description of NcmRepository
 *
 * @author victor
 */
class NcmRepository extends BaseRepository
{
    public $name = 'NCM';
    public $gender = 'o';

    public function __construct(Ncm $eloquent)
    {
        $this->eloquent = $eloquent;
    }
    
    public function beforeStore($attributes)
    {
//        $this->eloquent->company_id = \Auth::user()->company_id;

        
        return $attributes;
    }
    
    public function beforeUpdate($attributes)
    {
        if (!empty($attributes['value']))
        {
            $attributes['value'] = str_replace(['.', ','], ['', '.'], $attributes['value']);
        }
        
        return $attributes;
    }
}
