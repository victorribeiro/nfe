<?php

namespace App\Repositories;

use App\Models\Category;

/**
 * Description of CategoryRepository
 *
 * @author victor
 */
class CategoryRepository extends BaseRepository
{
    public $name = 'Categoria';
    public $gender = 'a';

    public function __construct(Category $eloquent)
    {
        $this->eloquent = $eloquent;
    }
    
    public function beforeStore($attributes)
    {
        $this->eloquent->company_id = \Auth::user()->company_id;

        return $attributes;
    }
}
