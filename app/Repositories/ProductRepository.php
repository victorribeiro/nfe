<?php

namespace App\Repositories;

use App\Models\Product;

/**
 * Description of ClientRepository
 *
 * @author victor
 */
class ProductRepository extends BaseRepository
{
    public $name = 'Produto';
    public $gender = 'o';

    public function __construct(Product $eloquent)
    {
        $this->eloquent = $eloquent;
    }
    
    public function beforeStore($attributes)
    {
//        $this->eloquent->company_id = \Auth::user()->company_id;

        if (!empty($attributes['value']))
        {
            $attributes['value'] = str_replace(['.', ','], ['', '.'], $attributes['value']);
        }
        
        return $attributes;
    }
    
    public function beforeUpdate($attributes)
    {
        if (!empty($attributes['value']))
        {
            $attributes['value'] = str_replace(['.', ','], ['', '.'], $attributes['value']);
        }
        
        return $attributes;
    }
}
