<?php

namespace App\Repositories;

use App\User;

class UserRepository extends BaseRepository
{
    public $name = 'Usuário';
    public $gender = 'o';

    public function __construct(User $eloquent)
    {
        $this->eloquent = $eloquent;
    }
    
    public function store($attributes)
    {
        $this->eloquent->fill( $this->beforeStore($attributes) );

        \DB::beginTransaction();
        if ($this->eloquent->save()) {
            $this->message(true, self::CREATED_MESSAGE);

            $this->afterStore($attributes);
        }
        else {
            $this->message(false, self::TRY_AGAIN_MESSAGE);
        }

        $this->success() ? \DB::commit() : \DB::rollback();

        return $this;
    }
}
