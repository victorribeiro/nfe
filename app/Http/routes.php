<?php

Route::get('/', 'HomeController@index');

Route::group(['prefix' => 'panel'], function() {
    Route::auth();
    
    Route::group(['middleware' => 'auth', ], function(){
        Route::get('', 'DashboardController@index');
        
        # Clients
        Route::group(['prefix' => 'clients'], function() {
            Route::get('', 'ClientsController@index');
            Route::get('create', 'ClientsController@create');
            Route::post('store', 'ClientsController@store');
            Route::get('edit/{id}', 'ClientsController@edit');
            Route::put('update/{id}', 'ClientsController@update');
            Route::get('destroy/{id}', 'ClientsController@destroy');
        });
        
        # Categories
        Route::group(['prefix' => 'categories'], function() {
            Route::get('', 'CategoriesController@index');
            Route::get('create', 'CategoriesController@create');
            Route::post('store', 'CategoriesController@store');
            Route::get('edit/{id}', 'CategoriesController@edit');
            Route::put('update/{id}', 'CategoriesController@update');
            Route::get('destroy/{id}', 'CategoriesController@destroy');
        });
        
        # Products
        Route::group(['prefix' => 'products'], function() {
            Route::get('', 'ProductsController@index');
            Route::get('create', 'ProductsController@create');
            Route::post('store', 'ProductsController@store');
            Route::get('edit/{id}', 'ProductsController@edit');
            Route::put('update/{id}', 'ProductsController@update');
            Route::get('destroy/{id}', 'ProductsController@destroy');
        });
        
        # Products
        Route::group(['prefix' => 'ncms'], function() {
            Route::get('', 'NcmsController@index');
            Route::get('create', 'NcmsController@create');
            Route::post('store', 'NcmsController@store');
            Route::get('edit/{id}', 'NcmsController@edit');
            Route::put('update/{id}', 'NcmsController@update');
            Route::get('destroy/{id}', 'NcmsController@destroy');
        });
        
        # Salesman
        Route::group(['prefix' => 'salesmans'], function() {
            Route::get('', 'SalesmansController@index');
            Route::get('create', 'SalesmansController@create');
            Route::post('store', 'SalesmansController@store');
            Route::get('edit/{id}', 'SalesmansController@edit');
            Route::put('update/{id}', 'SalesmansController@update');
            Route::get('destroy/{id}', 'SalesmansController@destroy');
        });
    });
    
});