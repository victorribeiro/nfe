<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ClientRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name' => ['required'],
            'document' => ['required'],
            'kind_person' => ['required'],
            'state' => ['alpha', 'size:2'],
        ];
    }

    public function messages()
    {
        return [
            'company_name.required' => 'O campo "Nome/Razão Social" é obrigatório.',
            'document.required' => 'O campo "CPF/CNPJ" é obrigatório.',
            'kind_person.required' => 'O campo "Tipo de Pessoa" é obrigatório.',
            'state.size' => 'O campo "Estado" deverá conter 2 caracteres.',
            'state.alpha' => 'O campo "Estado" deverá conter apenas letras.',
        ];
    }
}
