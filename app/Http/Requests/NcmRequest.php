<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NcmRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ncm' => ['required'],
            'cfop_id' => ['required'],
            'tax_situation_id' => ['required'],
            'state_id' => ['required'],
        ];
    }
    
    public function messages()
    {
        return [
            'ncm.required' => 'O campo "NCM" é obrigatório.',
            'cfop_id.required' => 'O campo "CFOP" é obrigatório.',
            'tax_situation_id.required' => 'O campo "Situação Tributária" é obrigatório.',
            'state_id.required' => 'O campo "Estado de Destino" é obrigatório.'
        ];
    }
}
