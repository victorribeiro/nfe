<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SalesmanRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required'],
            'document' => ['required'],
            'state' => ['alpha', 'size:2'],
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'O campo "Nome" é obrigatório.',
            'document.required' => 'O campo "CPF" é obrigatório.',
            'state.size' => 'O campo "Estado" deverá conter 2 caracteres.',
            'state.alpha' => 'O campo "Estado" deverá conter apenas letras.',
        ];
    }
}
