<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProductRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => ['required'],
            'name' => ['required'],
            'category_id' => ['required'],
            'unit_measure_id' => ['required'],
            'ncm' => ['required'],
            'value' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'code.required' => 'O campo "Código" é obrigatório.',
            'name.required' => 'O campo "Nome" é obrigatório.',
            'category_id.required' => 'O campo "Categoria" é obrigatório.',
            'unit_measure_id.required' => 'O campo "Unidade de medida" é obrigatório.',
            'ncm.required' => 'O campo "NCM" é obrigatório.',
            'value.required' => 'O campo "Valor" é obrigatório.',
        ];
    }
}
