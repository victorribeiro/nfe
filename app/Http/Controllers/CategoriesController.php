<?php

namespace App\Http\Controllers;

use App\Repositories\CategoryRepository;
use App\Http\Requests\CategoryRequest;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    protected $repository;
    
    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }
    
    public function index(Request $request)
    {   
        $categories = $this->repository->all();
        
        return view('categories.index', compact(['categories', 'request']));
    }
    
    public function create()
    {
        return view('categories.create');
    }
    
    public function store(CategoryRequest $request)
    {
        $categorie = $this->repository->store($request->all());
        
        if ($categorie->success()) {
            return redirect('panel/categories')->with('success', $categorie->message);
        } else {
            return redirect()->back()->with('error', $categorie->message);
        }
    }
    
    public function edit($id)
    {
        $category = $this->repository->find($id);
        
        return view('categories.edit', compact(['category']));
    }
    
    public function update($id, CategoryRequest $request, CategoryRepository $repository)
    {
        $updated = $repository->update($id, $request->all());
        
        if ($updated->success()) {
            return redirect('panel/categories')->with('success', $updated->message);
        } else {
            return redirect()->back()->with('error', $updated->message);
        }
    }
    
    public function destroy($id, CategoryRepository $repository)
    {
        $updated = $repository->destroy($id);
        
        if ($updated->success()) {
            return redirect('panel/categories')->with('success', $updated->message);
        } else {
            return redirect()->back()->with('error', $updated->message);
        }
    }
}
