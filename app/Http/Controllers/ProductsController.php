<?php

namespace App\Http\Controllers;

use App\Repositories\ProductRepository;
use App\Http\Requests\ProductRequest;
use App\Models\Cfop;
use App\Models\TaxSituation;
use App\Models\UnitMeasure;
use App\Models\ProductOrigin;
use App\Models\Category;

use Illuminate\Http\Request;

class ProductsController extends Controller
{
    protected $repository;
    
    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }
    
    public function index(Request $request)
    {   
        $products = $this->repository->all();
        
        return view('products.index', compact(['products', 'request']));
    }
    
    public function create()
    {
        $categories = Category::listing();
        $cfops = Cfop::listing();
        $taxSituations = TaxSituation::listing();
        $unitMeasures = UnitMeasure::listing();
        $productOrigins = ProductOrigin::listing();
        
        return view('products.create', compact(['cfops', 'taxSituations', 'unitMeasures', 'productOrigins', 'categories']));
    }
    
    public function store(Request $file, ProductRequest $request)
    {   
        if ($file->hasFile('image')) 
        {
            $imageName = $file->file('image')->getClientOriginalName();
            $file->file('image')->move(
                base_path() . '/public/files/uploads/products/', $imageName
            );
        }
        
        $product = $this->repository->store($request->all());
        
        if ($product->success()) 
        {
            return redirect('panel/products')->with('success', $product->message);
        } 
        else 
        {
            return redirect()->back()->with('error', $product->message);
        }
    }
    
    public function edit($id)
    {
        $product = $this->repository->find($id);
        $categories = Category::listing();
        $cfops = Cfop::listing();
        $taxSituations = TaxSituation::listing();
        $unitMeasures = UnitMeasure::listing();
        $productOrigins = ProductOrigin::listing();
        
        return view('products.edit', compact(['product', 'cfops', 'taxSituations', 'unitMeasures', 'productOrigins', 'categories']));
    }
    
    public function update(Request $file, $id, ProductRequest $request, ProductRepository $repository)
    {
        if ($file->hasFile('image')) 
        {
            $imageName = $file->file('image')->getClientOriginalName();
            $file->file('image')->move(
                base_path() . '/public/files/uploads/products/', $imageName
            );
        }
        else 
        {
            unset($request->all()['image']);
        }
        
        $updated = $repository->update($id, $request->all());
        
        if ($updated->success()) {
            return redirect('panel/products')->with('success', $updated->message);
        } else {
            return redirect()->back()->with('error', $updated->message);
        }
    }
    
    public function destroy($id, ProductRepository $repository)
    {
        $updated = $repository->destroy($id);
        
        if ($updated->success()) {
            return redirect('panel/products')->with('success', $updated->message);
        } else {
            return redirect()->back()->with('error', $updated->message);
        }
    }
}
