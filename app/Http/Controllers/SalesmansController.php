<?php

namespace App\Http\Controllers;

use App\Repositories\SalesmanRepository;
use App\Http\Requests\SalesmanRequest;
use App\Models\Country;

use Illuminate\Http\Request;

use App\Http\Requests;

class SalesmansController extends Controller
{
    protected $repository;
    
    public function __construct(SalesmanRepository $repository)
    {
        $this->repository = $repository;
    }
    
    public function index(Request $request)
    {   
        $salesmans = $this->repository->all();
        
        return view('salesmans.index', compact(['salesmans', 'request']));
    }
    
    public function create()
    {
        $countries = Country::listing();
        return view('salesmans.create', compact(['countries']));
    }
    
    public function store(SalesmanRequest $request)
    {
        $salesman = $this->repository->store($request->all());
        
        if ($salesman->success()) {
            return redirect('panel/salesmans')->with('success', $salesman->message);
        } else {
            return redirect()->back()->with('error', $salesman->message);
        }
    }
    
    public function edit($id)
    {
        $salesman = $this->repository->find($id);
        $countries = Country::listing();
        
        return view('salesmans.edit', compact(['salesman', 'countries']));
    }
    
    public function update($id, SalesmanRequest $request, SalesmanRepository $repository)
    {
        $updated = $repository->update($id, $request->all());
        
        if ($updated->success()) {
            return redirect('panel/salesmans')->with('success', $updated->message);
        } else {
            return redirect()->back()->with('error', $updated->message);
        }
    }
    
    public function destroy($id, SalesmanRepository $repository)
    {
        $updated = $repository->destroy($id);
        
        if ($updated->success()) {
            return redirect('panel/salesmans')->with('success', $updated->message);
        } else {
            return redirect()->back()->with('error', $updated->message);
        }
    }
}
