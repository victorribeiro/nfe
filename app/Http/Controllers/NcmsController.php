<?php

namespace App\Http\Controllers;

use App\Http\Requests\NcmRequest;
use App\Repositories\NcmRepository;

use App\Models\State;
use App\Models\Cfop;
use App\Models\TaxSituation;
use App\Models\CstPis;
use App\Models\CstIpi;

use Illuminate\Http\Request;

class NcmsController extends Controller
{
    protected $repository;
    
    public function __construct(NcmRepository $repository)
    {
        $this->repository = $repository;
    }
    
    public function index(Request $request)
    {   
        $ncms = $this->repository->all();
        
        return view('ncms.index', compact(['ncms', 'request']));
    }
    
    public function create()
    {
        $states = State::listing();
        $cfops = Cfop::listing();
        $taxSituations = TaxSituation::listing();
        $cstPis = CstPis::listing();
        $cstCofins = CstPis::listing();
        $cstIpi = CstIpi::listing();
        
        return view('ncms.create', compact(['states', 'cfops', 'taxSituations', 'cstPis', 'cstCofins', 'cstIpi']));
    }
    
    public function store(NcmRequest $request)
    {
        $ncm = $this->repository->store($request->all());
        
        if ($ncm->success()) {
            return redirect('panel/ncms')->with('success', $ncm->message);
        } else {
            return redirect()->back()->with('error', $ncm->message);
        }
    }
    
    public function edit($id)
    {
        $ncm = $this->repository->find($id);
        $states = State::listing();
        $cfops = Cfop::listing();
        $taxSituations = TaxSituation::listing();
        $cstPis = CstPis::listing();
        $cstIpi = CstIpi::listing();
        
        return view('ncms.edit', compact(['ncm', 'states', 'cfops', 'taxSituations', 'cstPis', 'cstIpi']));
    }
    
    public function update($id, NcmRequest $request, NcmRepository $repository)
    {
        $updated = $repository->update($id, $request->all());
        
        if ($updated->success()) {
            return redirect('panel/ncms')->with('success', $updated->message);
        } else {
            return redirect()->back()->with('error', $updated->message);
        }
    }
    
    public function destroy($id, NcmRepository $repository)
    {
        $updated = $repository->destroy($id);
        
        if ($updated->success()) {
            return redirect('panel/ncms')->with('success', $updated->message);
        } else {
            return redirect()->back()->with('error', $updated->message);
        }
    }
}
