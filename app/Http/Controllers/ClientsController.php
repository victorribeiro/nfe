<?php

namespace App\Http\Controllers;

use App\Repositories\ClientRepository;
use App\Http\Requests\ClientRequest;
use App\Models\Country;

use Illuminate\Http\Request;

class ClientsController extends Controller
{
    protected $repository;
    
    public function __construct(ClientRepository $repository)
    {
        $this->repository = $repository;
    }
    
    public function index(Request $request)
    {   
        $clients = $this->repository->all();
        
        return view('clients.index', compact(['clients', 'request']));
    }
    
    public function create()
    {
        $countries = Country::listing();
        return view('clients.create', compact(['countries']));
    }
    
    public function store(ClientRequest $request)
    {
        $client = $this->repository->store($request->all());
        
        if ($client->success()) {
            return redirect('panel/clients')->with('success', $client->message);
        } else {
            return redirect()->back()->with('error', $client->message);
        }
    }
    
    public function edit($id)
    {
        $client = $this->repository->find($id);
        $countries = Country::listing();
        
        return view('clients.edit', compact(['client', 'countries']));
    }
    
    public function update($id, ClientRequest $request, ClientRepository $repository)
    {
        $updated = $repository->update($id, $request->all());
        
        if ($updated->success()) {
            return redirect('panel/clients')->with('success', $updated->message);
        } else {
            return redirect()->back()->with('error', $updated->message);
        }
    }
    
    public function destroy($id, ClientRepository $repository)
    {
        $updated = $repository->destroy($id);
        
        if ($updated->success()) {
            return redirect('panel/clients')->with('success', $updated->message);
        } else {
            return redirect()->back()->with('error', $updated->message);
        }
    }
}
