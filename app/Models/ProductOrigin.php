<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class ProductOrigin extends BaseModel
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    public static function listing()
    {
        $banks = self::select(\DB::raw('TRIM(name) as name'), 'code', 'id')
            ->orderBy('code')
            ->get();

        $lists = [ '' => '' ];
        foreach ($banks as $key => $bank) {
            $lists[ $bank->id ] = $bank->code . ' - ' . ucwords(trim($bank->name));
        }

        return $lists;
    }
}
