<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends BaseModel
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    public static function listing()
    {
        $banks = self::select(\DB::raw('TRIM(name) as name'), 'id')
            ->orderBy('name')
            ->get();

        $lists = [ '' => '' ];
        foreach ($banks as $key => $bank) {
            $lists[ $bank->id ] = ucwords(trim($bank->name));
        }

        return $lists;
    }
}
