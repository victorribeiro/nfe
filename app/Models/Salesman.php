<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Salesman extends BaseModel
{
    use SoftDeletes;
    
    protected $fillable = [
        'company_id',
        'name',
        'document',
        'cep',
        'street',
        'number',
        'neighborhood',
        'complement',
        'city',
        'state',
        'countrie',
        'email',
        'phone',
        'note'
    ];
    
    protected $searchable = ['name'];
    
    protected $dates = ['deleted_at'];
    
    public static function listing()
    {
        $salesmans = self::select(\DB::raw('TRIM(name) as name'), 'id')
            ->orderBy('name')
            ->get();

        $lists = [ '' => '' ];
        foreach ($salesmans as $key => $salesman) {
            $lists[ $salesman->id ] = ucwords(trim($salesman->name));
        }

        return $lists;
    }
}
