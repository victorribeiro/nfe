<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Configuration extends BaseModel
{
    static $configurations;

    public static function firstBy($type, $name = null, $code = null)
    {
        return self::mount( Configuration::getVars($type, $name, $code)->first() );
    }

    public static function allBy($type, $name = null, $code = null)
    {
        $configs = Configuration::getVars($type, $name, $code)->get();

        $configurations = [];
        if (!$configs->isEmpty()) {
            foreach ($configs as $key => $config) {
                $configurations[] = self::mount($config);
            }
        }

        return $configurations;
    }

    public static function listsBy($type, $name = null, $code = null)
    {
        $configs = Configuration::getVars($type, $name, $code)->get();

        $configurations = [];
        if (!$configs->isEmpty()) {
            foreach ($configs as $key => $config) {
                $configurations[ $config->id ] = $config->content;
            }
        }

        return $configurations;
    }

    public static function scopeGetVars($query, $type, $name = null, $code = null)
    {
        $query->where('type', $type);
        if (!empty($name)) $query->where('name', $name);
        if (!empty($code)) $query->where('code', $code);

        return $query;
    }

    protected static function mount($config)
    {
        if (!empty($config)) {
            return (object) [ 'id' => $config->id, 'content' => $config->content ];
        }
    }
}
