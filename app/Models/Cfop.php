<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Cfop extends BaseModel
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    public static function listing()
    {
        $cfops = self::select(\DB::raw('TRIM(name) as name'), 'code', 'id')
            ->orderBy('code')
            ->get();

        $lists = [ '' => '' ];
        foreach ($cfops as $key => $cfop) {
            $lists[ $cfop->id ] = $cfop->code . ' - ' . ucwords(trim($cfop->name));
        }

        return $lists;
    }
}
