<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class CstIpi extends BaseModel
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    public static function listing()
    {
        $csts = self::select(\DB::raw('TRIM(name) as name'), 'code', 'id')
            ->orderBy('code')
            ->get();

        $lists = [ '' => '' ];
        foreach ($csts as $key => $cst) {
            $lists[ $cst->id ] = $cst->code . ' - ' . ucwords(trim($cst->name));
        }

        return $lists;
    }
}
