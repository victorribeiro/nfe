<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Ncm extends BaseModel
{
    protected $fillable = [
        'company_id',
        'ncm',
        'cfop_id',
        'tax_situation_id',
        'state_id',
        'aliquot_icms',
        'aliquot_icms_st',
        'aliquot_mva',
        'reduction_percentage_bc',
        'internal_aliquot_target',
        'reduction_percentage_bc_st',
        'percentage_acceptance',
        'credit_percentage_icms',
        'aliquot_pis',
        'cst_pis_id',
        'aliquot_cofins',
        'cst_cofins_id',
        'aliquot_ipi',
        'cst_ipi_id',
    ];
    
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    protected $searchable = ['ncm'];
    
    public function cfop()
    {
        return $this->belongsTo('App\Models\Cfop');
    }
    
    public function tax_situation()
    {
        return $this->belongsTo('App\Models\TaxSituation');
    }
    
    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }
}
