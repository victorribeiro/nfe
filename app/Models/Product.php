<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends BaseModel
{
    protected $fillable = [
        'company_id',
        'name',
        'code',
        'value',
        'ncm',
        'category_id',
        'unit_measure_id',
        'cfop_id',
        'tax_situation_id',
        'code_anp',
        'image',
        'product_origin_id'
    ];
    
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    protected $searchable = ['name'];
    
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }
    
    public function unitMeasures()
    {
        return $this->belongsTo('App\Models\UnitMeasure');
    }
    
    public function taxSituations()
    {
        return $this->belongsTo('App\Models\TaxSituation');
    }
    
    public function cfops()
    {
        return $this->belongsTo('App\Models\Cfop');
    }
}
