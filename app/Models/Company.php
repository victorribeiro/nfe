<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends BaseModel
{
    protected $fillable = [
        'kind_person',
        'company_name',
        'document',
        'municipal_registration',
        'state_registration',
        'cep',
        'street',
        'number',
        'neighborhood',
        'complement',
        'city',
        'state',
        'contact_name',
        'contact_email',
        'contact_phone',
        'counter_name',
        'counter_email',
        'counter_phone',
        'environment',
        'tax_regime',
        'nfce_token_csc',
        'nfce_csc',
        'nfce_serial_printer',
        'nfce_cfop_id',
        'certificate_a1',
        'certificate_a1_password',
        'logo',
        'integration_key',
        'plan_id',
        'card_number',
        'expiration_date',
        'cv',
        'card_name',
    ];

    use SoftDeletes;

    protected $dates = ['deleted_at'];
}
