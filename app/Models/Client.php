<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends BaseModel
{
    protected $fillable = [
        'company_id',
        'company_name',
        'fantasy_name',
        'document',
        'kind_person',
        'municipal_registration',
        'state_registration',
        'cep',
        'street',
        'number',
        'neighborhood',
        'complement',
        'city',
        'state',
        'countrie',
        'contact_name',
        'contact_email',
        'contact_phone'
    ];

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    protected $searchable = ['company_name', 'document'];
}
